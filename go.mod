module chainmaker.org/chainmaker/store-badgerdb/v2

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.2.1
	chainmaker.org/chainmaker/protocol/v2 v2.2.2
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
	github.com/yiyanwannian/badger/v3 v3.2111.29
)
